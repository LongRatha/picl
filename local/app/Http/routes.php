<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return view('pages.home');
});
Route::get('/about', function(){
  return view('pages.about');
});
Route::get('/program', function(){
  return view('pages.program');
});
Route::get('/news', function(){
  return view('pages.news');
});
Route::get('/information', function(){
  return view('pages.information');
});
Route::get('/career', function(){
  return view('pages.career');
});
Route::get('/contact', function(){
  return view('pages.contactus');
});
Route::get('/itec_program', function(){
  return view('pages.itec_program');
});
Route::get('/itec_website', function(){
  return view('pages.itec_website');
});
Route::get('/itec_network', function(){
  return view('pages.itec_network');
});
Route::get('/itec_design', function(){
  return view('pages.itec_design');
});
Route::get('/p-professional', function(){
  return view('pages.faculty-pages.p-professional');
});
Route::get('/p-schedule', function(){
  return view('pages.faculty-pages.p-schedule');
});
Route::get('/b-professional', function(){
  return view('pages.faculty-pages.b-professional');
});
Route::get('/b-schedule', function(){
  return view('pages.faculty-pages.b-schedule');
});
Route::get('/ma-professional', function(){
  return view('pages.faculty-pages.ma-professional');
});
Route::get('/ma-schedule', function(){
  return view('pages.faculty-pages.ma-schedule');
});
Route::get('/n-professional', function(){
  return view('pages.faculty-pages.n-professional');
});
Route::get('/n-schedule', function(){
  return view('pages.faculty-pages.n-schedule');
});
Route::get('/g-professional', function(){
  return view('pages.faculty-pages.g-professional');
});
Route::get('/g-schedule', function(){
  return view('pages.faculty-pages.g-schedule');
});
Route::get('/mu-professional', function(){
  return view('pages.faculty-pages.mu-professional');
});
Route::get('/mu-schedule', function(){
  return view('pages.faculty-pages.mu-schedule');
});
Route::get('/e-professional', function(){
  return view('pages.faculty-pages.e-professional');
});
Route::get('/e-schedule', function(){
  return view('pages.faculty-pages.e-schedule');
});
Route::get('/k-professional', function(){
  return view('pages.faculty-pages.k-professional');
});
Route::get('/k-schedule', function(){
  return view('pages.faculty-pages.k-schedule');
});
