@extends('layout.master')
@section('title')
  Home Page
@endsection
@section('img')
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <img src="img/picl_cover.jpg" class="img-responsive">
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
        <img src="img/programming.png" alt="..." width="250px" height="250px" class="pull-left img-responsive">
        <p class="titlehome">ការសរសេកម្មវិធី</p>
          <p> ហេតុអ្វីត្រូវរៀនការសរសេកម្មវិធីនៅ PICL ? ​សំរាប់អ្នកដែលចង់ចេះបង្កើតកម្មវិធី
            និងបង្កើតSystem ដើម្បីធ្វើការគ្រប់គ្រង
             បុគ្គលិកសិស្ស អវត្តមានសិស្ស ឬគ្រប់គ្រងនៅហាងលក់ទំនិញអ្វី  ឬគ្រប់គ្រងអ្វីផ្សេងទៀតនៅ
តាមស្ថាបន្ថ័នាៗ ។ហើយយើងនិងធ្វើការបង្រៀនតាំងពីកម្រិតតំបូង រហូតដល់កម្រិតខ្ពស់
 ជាមួយនិងគ្រូល្អមានបទពិសោធន៏រាប់ឆ្នាំ មិនតែប៉ុណ្ណោះ</p>
  </div>
  <div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
      <img src="img/networking.png" alt="..." width="250px" height="250px" class="pull-left">
      <p class="titlehome">ភ្ជាប់បណ្តាញ</p>
        <p>ដើម្បីក្លាយជាអ្នកគ្រប់គ្រងលើបណ្ដាញ Network
           ដ៏ពូកែម្នាក់យើងត្រូវសិក្សានូវមុខវិជ្ជាមួយចំនួនជាមុនសិន​ ជាពិសេសគឺមូលដ្ឋានគ្រឹះ Basic Network Window Sever 2012R2 MikroTiK Exchange Sever Redhat/Linux Cisso
និងមុខវិជ្ជាផ្សេងៗទៀតជាដើម ដោយសង្កេតមើលឃើញថាបច្ចុប្បន្ននេះនៅប្រទេសកម្ពុជាយើងកំពុងមានការរីកចម្រើនយ៉ាងខ្លាំងនៅផ្នែកបច្ចេកវិទ្យា
 ជាពិសេសនៅផ្នែកNetwork នេះជាដើមដែលមានក្រុមហ៊ុនធំៗត្រូវការជាច្រើនដូចច្នេះយើងត្រូវខំសិក្សានូវមុខជំនាញខាងលើជាពិសេស​ PICL នឹងផ្ដល់នូវជំនាញ Network ពិតប្រាកដសំរាប់អ្នក។</p>
</div>

<div class="col-lg-12 col-md-12 col-ms-12 col-xs-12">
    <img src="img/design.png" alt="..." width="250px" height="250px" class="pull-left">
    <p class="titlehome">កាត់តរូបភាព និងវីដេអូ</p>
      <p>អ្នកសិក្សានៅមហាវិទ្យាល័យ ឬ ជាអ្នកដែលចង់ចាប់យកជំនាញ Video Editer
        ដែលជាជំនាញពេញនិយមហើយក៏ទទួលបានប្រាក់ខែខ្គស់ដែរ ទៅតាមក្រុមហ៊ុន ទូរទស្សន៏ ឬក៏ផលិតកម្មផ្សេងៗ។ ដើម្បីក្លាយជា
         Video editer មួយរូបបានយើងត្រូវសិក្សាមុខវិជ្ជាមួយ ចំនួនដូចជា៖
Adobe Premiere, Sony vegas, video EDIUS Pro, Adobe After Effects+Plugin Ulean Cool 3D Studio</p>
</div>

  </div>
@endsection
