@extends('layout.master')
@section('title')
  Program
@endsection
@section('img')
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <img src="img/picl_cover.jpg" class="img-responsive">
    </div>
  </div>
@endsection
@section('content')
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 colo-xs-12">
      <h3 class="colortext">ថ្នាក់បរិញ្ញាបត្រវិស្វកម្មវិទ្យាសាស្រ្តកុំព្យូទ័រ</h3>
    <table class="table table-hover">
      <thead>
      <tr>
        <th class="bold">លរ</th>
        <th class="bold">ជំនាញ</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>១</td>
        <td><a href="{{url('/p-schedule')}}">សរសេរកម្មវិធី</a></td>
      </tr>
      <tr>
        <td>២</td>
        <td><a href="{{url('/n-schedule')}}">ភ្ជាប់បណ្តាញ</a></td>
      </tr>
      <tr>
        <td>៣</td>
        <td><a href="{{url('/b-schedule')}}" >សេដ្ឋកិច្ចព័ត៌មានវិទ្យា</a></td>
      </tr>
      <tr>
        <td>៤</td>
        <td><a href="{{url('/g-schedule')}}">កាត់តរូបភាព​ & វីដេអូ</a></td>
      </tr>
      <tr>
        <td>៥</td>
        <td><a href="{{url('/ma-schedule')}}">គ្រប់គ្រងប្រព័ន្ធព័ត៌មាន</a></td>
      </tr>
    </tbody>
  </table>
  <h3 class="colortext">ថ្នាក់បរិញ្ញាបត្រភាសាបរទេស</h3>
  <table class="table table-hover">
    <thead>
    <tr>
      <th class="bold">លរ</th>
      <th class="bold">ជំនាញ</th>
    </tr>
    <tbody>
      <tr>
        <td>១</td>
        <td><a href="{{url('/e-schedule')}}">អង់គ្លេស</a></td>
      </tr>
      <tr>
        <td>២</td>
        <td><a href="{{url('/ko-schedule')}}">កូរ៉េ</a></td>
      </tr>
    </tbody>
  </table>
      <hr />
    </div>
  </div>
@endsection
