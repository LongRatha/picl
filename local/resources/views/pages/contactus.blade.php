@extends('layout.master')
@section('title')
  Contact Us
@endsection
@section('img')
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <img src="img/picl_cover.jpg" class="img-responsive">
    </div>
  </div>
@endsection
@section('content')
  <center><h3>ទំនាក់ទំនង</h3></center>
  <i class="glyphicon glyphicon-map-marker i-color"></i> Building 143 | st 273 sangkat Toul,sangke khan Russey Keo,Phnom Penh
  <br>
  <i class="glyphicon glyphicon-earphone i-color"></i> Tel: 086 96 97 65 / 012 58 99 33 / 066 23 47 77
  <br>
  <i class="glyphicon glyphicon-globe i-color"></i> Website: www.picl-institute.com
  <br />
  <i class="glyphicon glyphicon-envelope i-color"></i> picl.edu@yahoo.com


<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 map-responsive">

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.5575801217246!2d104.90509341435897!3d11.583540791776912!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310951639b25b391%3A0x783bab32954f7d87!2sITEC!5e0!3m2!1sen!2skh!4v1492590495058" width="500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</div>
</div>
@endsection
