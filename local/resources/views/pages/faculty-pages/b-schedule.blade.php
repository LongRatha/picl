@extends('layout.master')
@section('title')
Prossional
@endsection

@section('content')
  <table class="table table-bordered table-condensed">
    <tr class="tb-back">
      <td colspan="5">Year I</td>
    </tr>
    <tr>
      <td colspan="5" class="tr-back">Semester I</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 101 --}}
    <tr>
      <td rowspan="2">EIT 101</td>
      <td rowspan="2">C Programming</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 102 --}}
    <tr>
      <td rowspan="2">EIT 102</td>
      <td rowspan="2">Basic Networking</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 103 --}}
    <tr>
      <td rowspan="2">EIT 103</td>
      <td rowspan="2">Computer Repaire</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 104 --}}
    <tr>
      <td rowspan="2">EIT 104</td>
      <td rowspan="2">Computer Technology</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 105 --}}
    <tr>
      <td rowspan="2">EIT 105</td>
      <td rowspan="2">Computer Application I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 106 --}}
    <tr>
      <td>EIT 106</td>
      <td>Englisch for IT I</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>

    {{-- semester II --}}

    <tr>
      <td colspan="5" class="tr-back">Semester II</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 201 --}}
    <tr>
      <td rowspan="2">EIT 201</td>
      <td rowspan="2">Introduction to Business</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 202 --}}
    <tr>
      <td rowspan="2">EIT 202</td>
      <td rowspan="2">Introduction to Management</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 203 --}}
    <tr>
      <td rowspan="2">EIT 203</td>
      <td rowspan="2">Introduction to Economic</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 204 --}}
    <tr>
      <td rowspan="2">EIT 204</td>
      <td rowspan="2">Database Application I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 205 --}}
    <tr>
      <td rowspan="2">EIT 205</td>
      <td rowspan="2">Computer Application II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 206 --}}
    <tr>
      <td>EIT 206</td>
      <td>English for IT II</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>
    <tr>
      <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
    </tr>

  {{-- year ii
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year II</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 301 --}}
  <tr>
  <td rowspan="2">EIT 301</td>
  <td rowspan="2">Advance of Business</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 302 --}}
  <tr>
  <td rowspan="2">EIT 302</td>
  <td rowspan="2">Advance of Management</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 303 --}}
  <tr>
  <td rowspan="2">ENG 303</td>
  <td rowspan="2">Network Infrastructure</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 304 --}}
  <tr>
  <td rowspan="2">EIT 304</td>
  <td rowspan="2">Database Application II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 305 --}}
  <tr>
  <td rowspan="2">EIT 305</td>
  <td rowspan="2">Project Management I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 306 --}}
  <tr>
  <td>EIT 306</td>
  <td>English for IT III</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 401 --}}
  <tr>
  <td rowspan="2">EIT 401</td>
  <td rowspan="2">Data Structure & Algorithm I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 402 --}}
  <tr>
  <td rowspan="2">EIT 402</td>
  <td rowspan="2">Statistic & Probability I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 403 --}}
  <tr>
  <td rowspan="2">EIT 403</td>
  <td rowspan="2">DBMS & Analysis</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 404 --}}
  <tr>
  <td rowspan="2">EIT 404</td>
  <td rowspan="2">Project Management II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 405 --}}
  <tr>
  <td rowspan="2">EIT 405</td>
  <td rowspan="2">English for IT III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 406 --}}
  <tr>
  <td>EIT 406</td>
  <td>Reserch & Methodology</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
  </tr>

  {{-- year III
  ----------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year III</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 501 --}}
  <tr>
  <td rowspan="2">EIT 501</td>
  <td rowspan="2">Organization IT Infrastructure </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 502 --}}
  <tr>
  <td rowspan="2">EIT 502</td>
  <td rowspan="2">Statistic & Probability II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 503 --}}
  <tr>
  <td rowspan="2">EIT 503</td>
  <td rowspan="2">System Analysis I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 504 --}}
  <tr>
  <td rowspan="2">EIT 504</td>
  <td rowspan="2">ERP(Enterprise Resource Planning) System I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis505 --}}
  <tr>
  <td rowspan="2">EIT 505</td>
  <td rowspan="2">English for Business I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>

  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 601 --}}
  <tr>
  <td rowspan="2">EIT 601</td>
  <td rowspan="2">E-Business System I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 602 --}}
  <tr>
  <td rowspan="2">EIT 602</td>
  <td rowspan="2">Innovating </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 603 --}}
  <tr>
  <td rowspan="2">EIT 603</td>
  <td rowspan="2">Business Communication II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 604 --}}
  <tr>
  <td rowspan="2">EIT 604</td>
  <td rowspan="2">Listening Skill II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 605 --}}
  <tr>
  <td rowspan="2">EIT 605</td>
  <td rowspan="2">English for Business II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=30 CREDITS(THEORY 14 CREDITS, PRACTICE=16 CREDITS) & 690 HOURS</td>
  </tr>

  {{-- year IV
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year IV</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 701 --}}
  <tr>
  <td rowspan="2">EIT 701</td>
  <td rowspan="2">E-Business System II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 702 --}}
  <tr>
  <td rowspan="2">EIT 702</td>
  <td rowspan="2">Social Media in Organization I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 703 --}}
  <tr>
  <td rowspan="2">EIT 703</td>
  <td rowspan="2">Information Risk Management and Security I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 704 --}}
  <tr>
  <td rowspan="2">EIT 704</td>
  <td rowspan="2">Building Information System Technology I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 705 --}}
  <tr>
  <td rowspan="2">EIT 705</td>
  <td rowspan="2">Marketing Management & Research I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>5</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 801 --}}
  <tr>
  <td rowspan="2">EIT 801</td>
  <td rowspan="2">Social Analysis with Business</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 802 --}}
  <tr>
  <td rowspan="2">EIT 802</td>
  <td rowspan="2">Social Media in Organizations</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis803 --}}
  <tr>
  <td rowspan="2">EIT 803</td>
  <td rowspan="2">Information Risk Management and Security II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 804 --}}
  <tr>
  <td rowspan="2">EIT 804</td>
  <td rowspan="2">Building Information System Technology II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 805 --}}
  <tr>
  <td rowspan="2">EIT 805</td>
  <td rowspan="2">Marketing Mangement & Research II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 806 --}}
  <tr>
  <td>EIT 806</td>
  <td>Intership</td>
  <td>135</td>
  <td>0</td>
  <td>3</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>510</td>
  <td>5</td>
  <td>13</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=33 CREDITS(THEORY 10 CREDITS, PRACTICE=23 CREDITS) & 885 HOURS</td>
  </tr>
  <tr>
  <td colspan="5">GRADE TOTAL=135 CREDITS</td>
  </tr>
  </table>
@endsection
