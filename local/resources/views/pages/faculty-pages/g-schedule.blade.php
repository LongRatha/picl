@extends('layout.master')
@section('title')
Prossional
@endsection

@section('content')
  <table class="table table-condensed table-bordered">
    <tr class="tb-back">
      <td colspan="5">Year I</td>
    </tr>
    <tr>
      <td colspan="5" class="tr-back">Semester I</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 101 --}}
    <tr>
      <td rowspan="2">DES 101</td>
      <td rowspan="2">C Programming</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 102 --}}
    <tr>
      <td rowspan="2">DES 102</td>
      <td rowspan="2">Basic Networking</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 103 --}}
    <tr>
      <td rowspan="2">DES 103</td>
      <td rowspan="2">Computer Repaire</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 104 --}}
    <tr>
      <td rowspan="2">DES 104</td>
      <td rowspan="2">Computer Technology</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 105 --}}
    <tr>
      <td rowspan="2">DES 105</td>
      <td rowspan="2">Computer Application I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 106 --}}
    <tr>
      <td>DES 106</td>
      <td>Englisch for IT I</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>

    {{-- semester II --}}

    <tr>
      <td colspan="5" class="tr-back">Semester II</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 201 --}}
    <tr>
      <td rowspan="2">DES 201</td>
      <td rowspan="2">Programming Technique</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 202 --}}
    <tr>
      <td rowspan="2">DES 202</td>
      <td rowspan="2">Digital Logic I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 203 --}}
    <tr>
      <td rowspan="2">DES 203</td>
      <td rowspan="2">Technology & Information System I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 204 --}}
    <tr>
      <td rowspan="2">DES 204</td>
      <td rowspan="2">Mathematics for Computer Graphics</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 205 --}}
    <tr>
      <td rowspan="2">DES 205</td>
      <td rowspan="2">Artificial Intelligence I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 206 --}}
    <tr>
      <td>DES 206</td>
      <td>English for IT II</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>
    <tr>
      <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
    </tr>

  {{-- year ii
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year II</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 301 --}}
  <tr>
  <td rowspan="2">DES 301</td>
  <td rowspan="2">Programming Technique II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 302 --}}
  <tr>
  <td rowspan="2">DES 302</td>
  <td rowspan="2">Digital Logic II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 303 --}}
  <tr>
  <td rowspan="2">DES 303</td>
  <td rowspan="2">Technology & Information System II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 304 --}}
  <tr>
  <td rowspan="2">DES 304</td>
  <td rowspan="2">Mathimatics for Computer Graphic II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 305 --}}
  <tr>
  <td rowspan="2">DES 305</td>
  <td rowspan="2">Artificial Intelligence II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 306 --}}
  <tr>
  <td>DES 306</td>
  <td>English for IT III</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 401 --}}
  <tr>
  <td rowspan="2">DES 401</td>
  <td rowspan="2">Programming Technique III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 402 --}}
  <tr>
  <td rowspan="2">DES 402</td>
  <td rowspan="2">Computer Graphic I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 403 --}}
  <tr>
  <td rowspan="2">DES 403</td>
  <td rowspan="2">Multimadia Graphic I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 404 --}}
  <tr>
  <td rowspan="2">DES 404</td>
  <td rowspan="2">Mathematics for Computer Graphics III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 405 --}}
  <tr>
  <td rowspan="2">DES 405</td>
  <td rowspan="2">Artificial Intelligence III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 406 --}}
  <tr>
  <td>DES 406</td>
  <td>Research & Methodogy</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
  </tr>

  {{-- year III
  ----------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year III</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 501 --}}
  <tr>
  <td rowspan="2">DES 501</td>
  <td rowspan="2">Computer Graphic II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 502 --}}
  <tr>
  <td rowspan="2">DES 502</td>
  <td rowspan="2">Multimedia Design II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 503 --}}
  <tr>
  <td rowspan="2">DES 503</td>
  <td rowspan="2">Modeling & Semulation I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 504 --}}
  <tr>
  <td rowspan="2">DES 504</td>
  <td rowspan="2">Multimedia Web Programming</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis505 --}}
  <tr>
  <td rowspan="2">DES 505</td>
  <td rowspan="2">English for Business I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>

  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 601 --}}
  <tr>
  <td rowspan="2">DES 601</td>
  <td rowspan="2">Interactive Computer Graphic I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 602 --}}
  <tr>
  <td rowspan="2">DES 602</td>
  <td rowspan="2">Fundamental of Image Processing</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 603 --}}
  <tr>
  <td rowspan="2">DES 603</td>
  <td rowspan="2">Modeling & Semulation II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 604 --}}
  <tr>
  <td rowspan="2">DES 604</td>
  <td rowspan="2">Multimedia Web Programming II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 605 --}}
  <tr>
  <td rowspan="2">DES 605</td>
  <td rowspan="2">English for Business II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=30 CREDITS(THEORY 14 CREDITS, PRACTICE=16 CREDITS) & 690 HOURS</td>
  </tr>

  {{-- year IV
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year IV</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 701 --}}
  <tr>
  <td rowspan="2">DES 701</td>
  <td rowspan="2">Interactive Multimedia Development</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 702 --}}
  <tr>
  <td rowspan="2">DES 702</td>
  <td rowspan="2">Data Visualization I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 703 --}}
  <tr>
  <td rowspan="2">DES 703</td>
  <td rowspan="2">Advance Computer Graphic</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 704 --}}
  <tr>
  <td rowspan="2">DES 704</td>
  <td rowspan="2">Interactive Computer Graphic</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 705 --}}
  <tr>
  <td rowspan="2">DES 705</td>
  <td rowspan="2">Computer Graphic Information System I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>5</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 801 --}}
  <tr>
  <td rowspan="2">DES 801</td>
  <td rowspan="2">ICT for Development</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 802 --}}
  <tr>
  <td rowspan="2">DES 802</td>
  <td rowspan="2">Data Visualization II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis803 --}}
  <tr>
  <td rowspan="2">DES 803</td>
  <td rowspan="2">Multimedia Data Processing</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 804 --}}
  <tr>
  <td rowspan="2">DES 804</td>
  <td rowspan="2">Intelligence Multimedia Processing</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 805 --}}
  <tr>
  <td rowspan="2">DES 805</td>
  <td rowspan="2">Computer Graphics Information System II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 806 --}}
  <tr>
  <td>DES 806</td>
  <td>Intership</td>
  <td>135</td>
  <td>0</td>
  <td>3</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>510</td>
  <td>5</td>
  <td>13</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=33 CREDITS(THEORY 10 CREDITS, PRACTICE=23 CREDITS) & 855 HOURS</td>
  </tr>
  <tr>
  <td colspan="5">GRADE TOTAL=135 CREDITS</td>
  </tr>
  </table>
@endsection
