@extends('layout.master')
@section('title')
  Prossional
@endsection

@section('content')
    <div class="table-responsive">
      <table class="table table-bordered table-condensed">
        <tr class="tb-back">
          <td colspan="5">Year I</td>
        </tr>
        <tr>
          <td colspan="5" class="tr-back">Semester I</td>
        </tr>
        <tr>
          <td rowspan="2">N<sup>o</sup></td>
          <td rowspan="2">Subject</td>
          <td rowspan="2">Hour</td>
          <td colspan="2">Credit</td>
        </tr>
        <tr>
          <td>Theory</td>
          <td>Practice</td>
        </tr>
        {{-- mis 101 --}}
        <tr>
          <td rowspan="2" style="vertical-align: middle">Pro 101</td>
          <td rowspan="2">C Programing</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 102 --}}
        <tr>
          <td rowspan="2">Pro 102</td>
          <td rowspan="2">Basic Networking</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 103 --}}
        <tr>
          <td rowspan="2">Pro 103</td>
          <td rowspan="2">Computer Repair</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 104 --}}
        <tr>
          <td rowspan="2">Pro 104</td>
          <td rowspan="2">Computer Technology</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 105 --}}
        <tr>
          <td rowspan="2">Pro 105</td>
          <td rowspan="2">Computer Application I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 106 --}}
        <tr>
          <td>Pro 106</td>
          <td>English for IT I</td>
          <td>45</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <td colspan="2">Total:</td>
          <td>420</td>
          <td>8</td>
          <td>10</td>
        </tr>

        {{-- semester II --}}

        <tr>
          <td colspan="5" class="tr-back">Semester II</td>
        </tr>
        <tr>
          <td rowspan="2">N<sup>o</sup></td>
          <td rowspan="2">Subject</td>
          <td rowspan="2">Hour</td>
          <td colspan="2">Credit</td>
        </tr>
        <tr>
          <td>Theory</td>
          <td>Practice</td>
        </tr>
        {{-- mis 201 --}}
        <tr>
          <td rowspan="2">Pro 201</td>
          <td rowspan="2">C++ Programming I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 202 --}}
        <tr>
          <td rowspan="2">Pro 202</td>
          <td rowspan="2">Web Designe (HTML & CSS)</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 203 --}}
        <tr>
          <td rowspan="2">Pro 203</td>
          <td rowspan="2">Database Application & DBMS I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 204 --}}
        <tr>
          <td rowspan="2">Pro 204</td>
          <td rowspan="2">Computer Application II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 205 --}}
        <tr>
          <td rowspan="2">Pro 205</td>
          <td rowspan="2">UI & UX</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 206 --}}
        <tr>
          <td>Pro 206</td>
          <td>English for IT II</td>
          <td>45</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <td colspan="2">Total:</td>
          <td>420</td>
          <td>8</td>
          <td>10</td>
        </tr>
        <tr>
          <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 480 HOURS</td>
        </tr>

        {{-- year ii
        --------------------------------------------------------------------------------- --}}
        <tr class="tb-back">
          <td colspan="5">Year II</td>
        </tr>
        <tr>
          <td colspan="5" class="tr-back">Semester I</td>
        </tr>
        <tr>
          <td rowspan="2">N<sup>o</sup></td>
          <td rowspan="2">Subject</td>
          <td rowspan="2">Hour</td>
          <td colspan="2">Credit</td>
        </tr>
        <tr>
          <td>Theory</td>
          <td>Practice</td>
        </tr>
        {{-- mis 301 --}}
        <tr>
          <td rowspan="2">Pro 301</td>
          <td rowspan="2">C++ Programming II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 302 --}}
        <tr>
          <td rowspan="2">Pro 302</td>
          <td rowspan="2">Web Design (Javascript & Jquery)</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 303 --}}
        <tr>
          <td rowspan="2">Pro 303</td>
          <td rowspan="2">Dabase Application & DBMS II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 304 --}}
        <tr>
          <td rowspan="2">Pro 304</td>
          <td rowspan="2">C# Programming I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 305 --}}
        <tr>
          <td rowspan="2">Pro 305</td>
          <td rowspan="2">Data Structure & Algorithm I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 306 --}}
        <tr>
          <td>Pro 306</td>
          <td>English for IT III</td>
          <td>45</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <td colspan="2">Total:</td>
          <td>420</td>
          <td>8</td>
          <td>10</td>
        </tr>

        {{-- semester II --}}

        <tr>
          <td colspan="5" class="tr-back">Semester II</td>
        </tr>
        <tr>
          <td rowspan="2">N<sup>o</sup></td>
          <td rowspan="2">Subject</td>
          <td rowspan="2">Hour</td>
          <td colspan="2">Credit</td>
        </tr>
        <tr>
          <td>Theory</td>
          <td>Practice</td>
        </tr>
        {{-- mis 401 --}}
        <tr>
          <td rowspan="2">Pro 401</td>
          <td rowspan="2">Wev Development (PHP & MySQL)</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 402 --}}
        <tr>
          <td rowspan="2">Pro 402</td>
          <td rowspan="2">C# Programming II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 403 --}}
        <tr>
          <td rowspan="2">Pro 403</td>
          <td rowspan="2">Data Structure & Algorithm II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 404 --}}
        <tr>
          <td rowspan="2">Pro 404</td>
          <td rowspan="2">English for IT IV</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 405 --}}
        <tr>
          <td rowspan="2">Pro 405</td>
          <td rowspan="2">Research & Methodology</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 406 --}}
        <tr>
          <td>Pro 406</td>
          <td>Project Researc</td>
          <td>45</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <td colspan="2">Total:</td>
          <td>420</td>
          <td>8</td>
          <td>10</td>
        </tr>
        <tr>
          <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
        </tr>

        {{-- year III
        ----------------------------------------------------------------------- --}}
        <tr class="tb-back">
          <td colspan="5">Year III</td>
        </tr>
        <tr>
          <td colspan="5" class="tr-back">Semester I</td>
        </tr>
        <tr>
          <td rowspan="2">N<sup>o</sup></td>
          <td rowspan="2">Subject</td>
          <td rowspan="2">Hour</td>
          <td colspan="2">Credit</td>
        </tr>
        <tr>
          <td>Theory</td>
          <td>Practice</td>
        </tr>
        {{-- mis 501 --}}
        <tr>
          <td rowspan="2">Pro 501</td>
          <td rowspan="2">Web Development (PHP)</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 502 --}}
        <tr>
          <td rowspan="2">Pro 502</td>
          <td rowspan="2">C# Programming with Project</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 503 --}}
        <tr>
          <td rowspan="2">Pro 503</td>
          <td rowspan="2">System Analysis I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 504 --}}
        <tr>
          <td rowspan="2">Pro 504</td>
          <td rowspan="2">Java Programming I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis505 --}}
        <tr>
          <td>Pro 505</td>
          <td>English for IT V</td>
          <td>45</td>
          <td>3</td>
          <td>0</td>
        </tr>
        <tr>
          <td colspan="2">Total:</td>
          <td>345</td>
          <td>7</td>
          <td>8</td>
        </tr>

        {{-- semester II --}}

        <tr>
          <td colspan="5" class="tr-back">Semester II</td>
        </tr>
        <tr>
          <td rowspan="2">N<sup>o</sup></td>
          <td rowspan="2">Subject</td>
          <td rowspan="2">Hour</td>
          <td colspan="2">Credit</td>
        </tr>
        <tr>
          <td>Theory</td>
          <td>Practice</td>
        </tr>
        {{-- mis 601 --}}
        <tr>
          <td rowspan="2">Pro 601</td>
          <td rowspan="2">Web Development with ASP.NET I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 602 --}}
        <tr>
          <td rowspan="2">Pro 602</td>
          <td rowspan="2">SQL Server Management I</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 603 --}}
        <tr>
          <td rowspan="2">Pro 603</td>
          <td rowspan="2">System Analysis II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 604 --}}
        <tr>
          <td rowspan="2">Pro 604</td>
          <td rowspan="2">Java Programming II</td>
          <td>15</td>
          <td>1</td>
          <td>0</td>
        </tr>
        <tr>
          <td>60</td>
          <td>0</td>
          <td>2</td>
        </tr>

        {{-- mis 605 --}}
        <tr>
          <td rowspan="2">Pro 605</td>
          <td rowspan="2">English for IT VI</td>
          <tr>
            <td>45</td>
            <td>3</td>
            <td>0</td>
          </tr>
          <tr>
            <td colspan="2">Total:</td>
            <td>345</td>
            <td>7</td>
            <td>8</td>
          </tr>
          <tr>
            <td colspan="5">TOTAL=30 CREDITS(THEORY 14 CREDITS, PRACTICE=16 CREDITS) & 690 HOURS</td>
          </tr>

          {{-- year IV
          --------------------------------------------------------------------------------- --}}
          <tr class="tb-back">
            <td colspan="5">Year IV</td>
          </tr>
          <tr>
            <td colspan="5" class="tr-back">Semester I</td>
          </tr>
          <tr>
            <td rowspan="2">N<sup>o</sup></td>
            <td rowspan="2">Subject</td>
            <td rowspan="2">Hour</td>
            <td colspan="2">Credit</td>
          </tr>
          <tr>
            <td>Theory</td>
            <td>Practice</td>
          </tr>
          {{-- mis 701 --}}
          <tr>
            <td rowspan="2">Pro 701</td>
            <td rowspan="2">SQL Server Management II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 702 --}}
          <tr>
            <td rowspan="2">Pro 702</td>
            <td rowspan="2">Basic Android APP I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 703 --}}
          <tr>
            <td rowspan="2">Pro 703</td>
            <td rowspan="2">SPSS & Analysis Data I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 704 --}}
          <tr>
            <td rowspan="2">Pro 704</td>
            <td rowspan="2">Web Development with ASP.NET I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 705 --}}
          <tr>
            <td rowspan="2">Pro 705</td>
            <td rowspan="2">Java Programming with JSP</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>
          <tr>
            <td colspan="2">Total:</td>
            <td>375</td>
            <td>5</td>
            <td>10</td>
          </tr>

          {{-- semester II --}}

          <tr>
            <td colspan="5" class="tr-back">Semester II</td>
          </tr>
          <tr>
            <td rowspan="2">N<sup>o</sup></td>
            <td rowspan="2">Subject</td>
            <td rowspan="2">Hour</td>
            <td colspan="2">Credit</td>
          </tr>
          <tr>
            <td>Theory</td>
            <td>Practice</td>
          </tr>
          {{-- mis 801 --}}
          <tr>
            <td rowspan="2">Pro 801</td>
            <td rowspan="2">Oracle Database Management I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 802 --}}
          <tr>
            <td rowspan="2">Pro 802</td>
            <td rowspan="2">Advance Android app II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis803 --}}
          <tr>
            <td rowspan="2">Pro 803</td>
            <td rowspan="2">SPSS & Analysis Data II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 804 --}}
          <tr>
            <td rowspan="2">Pro 804</td>
            <td rowspan="2">Web Development with ASP.NET II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 805 --}}
          <tr>
            <td rowspan="2">Pro 805</td>
            <td rowspan="2">Basic IOS</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>
          <tr>
            <td colspan="2">Total:</td>
            <td>375</td>
            <td>5</td>
            <td>10</td>
          </tr>
          <tr>
            <td colspan="5">TOTAL=30 CREDITS(THEORY 10 CREDITS, PRACTICE=20 CREDITS) & 750 HOURS</td>
          </tr>
          {{-- year V
          --------------------------------------------------------------------------------- --}}
          <tr class="tb-back">
            <td colspan="5">Year V</td>
          </tr>
          <tr>
            <td colspan="5" class="tr-back">Semester I</td>
          </tr>
          <tr>
            <td rowspan="2">N<sup>o</sup></td>
            <td rowspan="2">Subject</td>
            <td rowspan="2">Hour</td>
            <td colspan="2">Credit</td>
          </tr>
          <tr>
            <td>Theory</td>
            <td>Practice</td>
          </tr>
          {{-- mis 701 --}}
          <tr>
            <td rowspan="2">Pro 901</td>
            <td rowspan="2">Oracle Database Management II </td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 702 --}}
          <tr>
            <td rowspan="2">Pro 902</td>
            <td rowspan="2">Fundamental for IOS I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 703 --}}
          <tr>
            <td rowspan="2">Pro 903</td>
            <td rowspan="2">MCS & Wordpress I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 704 --}}
          <tr>
            <td rowspan="2">Pro 904</td>
            <td rowspan="2">E-Commerce Technology I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 705 --}}
          <tr>
            <td rowspan="2">Pro 905</td>
            <td rowspan="2">Software Engineering I</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>
          <tr>
            <td colspan="2">Total:</td>
            <td>375</td>
            <td>5</td>
            <td>10</td>
          </tr>

          {{-- semester II --}}

          <tr>
            <td colspan="5" class="tr-back">Semester II</td>
          </tr>
          <tr>
            <td rowspan="2">N<sup>o</sup></td>
            <td rowspan="2">Subject</td>
            <td rowspan="2">Hour</td>
            <td colspan="2">Credit</td>
          </tr>
          <tr>
            <td>Theory</td>
            <td>Practice</td>
          </tr>
          {{-- mis 801 --}}
          <tr>
            <td rowspan="2">Pro 1001</td>
            <td rowspan="2">Advance of Basic IOS II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 802 --}}
          <tr>
            <td rowspan="2">Pro 1002</td>
            <td rowspan="2">Sottware Engineering II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis803 --}}
          <tr>
            <td rowspan="2">Pro 1003</td>
            <td rowspan="2">CMS & Wordpress II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 804 --}}
          <tr>
            <td rowspan="2">Pro 1004</td>
            <td rowspan="2">ICT for Development</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>

          {{-- mis 805 --}}
          <tr>
            <td rowspan="2">Pro 1005</td>
            <td rowspan="2">E-commerce Technology II</td>
            <td>15</td>
            <td>1</td>
            <td>0</td>
          </tr>
          <tr>
            <td>60</td>
            <td>0</td>
            <td>2</td>
          </tr>
          {{-- mis 805 --}}
          <tr>
            <td>Pro 1005</td>
            <td>Internship</td>
            <td>135</td>
            <td>0</td>
            <td>3</td>
          </tr>
          <tr>
            <td colspan="2">Total:</td>
            <td>510</td>
            <td>5</td>
            <td>13</td>
          </tr>
          <tr>
            <td colspan="5">TOTAL=33 CREDITS(THEORY 10 CREDITS, PRACTICE=23 CREDITS) & 885 HOURS</td>
          </tr>
          <tr>
            <td colspan="5">GRADE TOTAL=165 CREDITS</td>
          </tr>
        </table>
      </div>
    @endsection
