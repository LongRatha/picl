@extends('layout.master')
@section('title')
Prossional
@endsection

@section('content')
  <table class="table table-bordered table-condensed">
    <tr class="tb-back">
      <td colspan="5">Year I</td>
    </tr>
    <tr>
      <td colspan="5" class="tr-back">Semester I</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 101 --}}
    <tr>
      <td rowspan="2">ENG 101</td>
      <td rowspan="2">Basic of Computer I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 102 --}}
    <tr>
      <td rowspan="2">ENG 102</td>
      <td rowspan="2">Computer Application I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 103 --}}
    <tr>
      <td rowspan="2">ENG 103</td>
      <td rowspan="2">Practical English in use I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 104 --}}
    <tr>
      <td rowspan="2">ENG 104</td>
      <td rowspan="2">Introduction to Appreciation of English Leterature I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 105 --}}
    <tr>
      <td rowspan="2">ENG 105</td>
      <td rowspan="2">English Composition I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 106 --}}
    <tr>
      <td>MIS 106</td>
      <td>Fundamental Computer I</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>450</td>
      <td>6</td>
      <td>12</td>
    </tr>

    {{-- semester II --}}

    <tr>
      <td colspan="5" class="tr-back">Semester II</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 201 --}}
    <tr>
      <td rowspan="2">ENG 201</td>
      <td rowspan="2">Basic of Computer II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 202 --}}
    <tr>
      <td rowspan="2">ENG 202</td>
      <td rowspan="2">Computer Application II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 203 --}}
    <tr>
      <td rowspan="2">ENG 203</td>
      <td rowspan="2">Practical English in use II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 204 --}}
    <tr>
      <td rowspan="2">ENG 204</td>
      <td rowspan="2">Introduction to the Appreciation of English Leterature II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 205 --}}
    <tr>
      <td rowspan="2">ENG 205</td>
      <td rowspan="2">English Compositon II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 206 --}}
    <tr>
      <td>MIS 206</td>
      <td>Fundamental Computer II</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>450</td>
      <td>6</td>
      <td>12</td>
    </tr>
    <tr>
      <td colspan="5">TOTAL=36 CREDITS(THEORY 12 CREDITS, PRACTICE=24 CREDITS) & 900 HOURS</td>
    </tr>

  {{-- year ii
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year II</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 301 --}}
  <tr>
  <td rowspan="2">ENG 301</td>
  <td rowspan="2">Computer Communication I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 302 --}}
  <tr>
  <td rowspan="2">ENG 302</td>
  <td rowspan="2">College English I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 303 --}}
  <tr>
  <td rowspan="2">ENG 303</td>
  <td rowspan="2">Introduction to Statistic</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 304 --}}
  <tr>
  <td rowspan="2">ENG 304</td>
  <td rowspan="2">Critical Reading & Writing I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 305 --}}
  <tr>
  <td rowspan="2">ENG 305</td>
  <td rowspan="2">Practical English in use III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 306 --}}
  <tr>
  <td>MIS 306</td>
  <td>English for IT</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 401 --}}
  <tr>
  <td rowspan="2">ENG 401</td>
  <td rowspan="2">Computer Communication II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 402 --}}
  <tr>
  <td rowspan="2">ENG 402</td>
  <td rowspan="2">College English II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 403 --}}
  <tr>
  <td rowspan="2">ENG 403</td>
  <td rowspan="2">Fundamental of Statistic</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 404 --}}
  <tr>
  <td rowspan="2">ENG 404</td>
  <td rowspan="2">Critical Reading & Writing II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 405 --}}
  <tr>
  <td rowspan="2">ENG 405</td>
  <td rowspan="2">Advanced English Grammar</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 406 --}}
  <tr>
  <td>MIS 406</td>
  <td>Computer Communication II</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
  </tr>

  {{-- year III
  ----------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year III</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 501 --}}
  <tr>
  <td rowspan="2">ENG 501</td>
  <td rowspan="2">Spoken English I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 502 --}}
  <tr>
  <td rowspan="2">ENG 502</td>
  <td rowspan="2">Writing & Academic Purpose I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 503 --}}
  <tr>
  <td rowspan="2">ENG 503</td>
  <td rowspan="2">Business Communication I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 504 --}}
  <tr>
  <td rowspan="2">ENG 504</td>
  <td rowspan="2">Listening Skill I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis505 --}}
  <tr>
  <td rowspan="2">ENG 505</td>
  <td rowspan="2">English for Business I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>

  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 601 --}}
  <tr>
  <td rowspan="2">ENG 601</td>
  <td rowspan="2">Spoken English II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 602 --}}
  <tr>
  <td rowspan="2">ENG 602</td>
  <td rowspan="2">Writing for Academic Purpose II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 603 --}}
  <tr>
  <td rowspan="2">ENG 603</td>
  <td rowspan="2">Business Communication II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 604 --}}
  <tr>
  <td rowspan="2">ENG 604</td>
  <td rowspan="2">Listening Skill II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 605 --}}
  <tr>
  <td rowspan="2">ENG 605</td>
  <td rowspan="2">English for Business II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=30 CREDITS(THEORY 14 CREDITS, PRACTICE=16 CREDITS) & 690 HOURS</td>
  </tr>

  {{-- year IV
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year IV</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 701 --}}
  <tr>
  <td rowspan="2">ENG 701</td>
  <td rowspan="2">Asian English Leterature </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 702 --}}
  <tr>
  <td rowspan="2">ENG 702</td>
  <td rowspan="2">Foundation of Education</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 703 --}}
  <tr>
  <td rowspan="2">ENG 703</td>
  <td rowspan="2">Teaching English as a Forein Language</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 704 --}}
  <tr>
  <td rowspan="2">ENG 704</td>
  <td rowspan="2">Global Studies</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 705 --}}
  <tr>
  <td rowspan="2">ENG 705</td>
  <td rowspan="2">English for Management I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>375</td>
  <td>5</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 801 --}}
  <tr>
  <td rowspan="2">ENG 801</td>
  <td rowspan="2">Reserach Methodology and Apply</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 802 --}}
  <tr>
  <td rowspan="2">ENG 802</td>
  <td rowspan="2">Leterature and Society II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis803 --}}
  <tr>
  <td rowspan="2">ENG 803</td>
  <td rowspan="2">Advanced Business Communication</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 804 --}}
  <tr>
  <td rowspan="2">ENG 804</td>
  <td rowspan="2">Introduction to Linguistics</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 805 --}}
  <tr>
  <td rowspan="2">ENG 805</td>
  <td rowspan="2">English for Management I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 806 --}}
  <tr>
  <td>MIS 806</td>
  <td>Inership</td>
  <td>135</td>
  <td>0</td>
  <td>3</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>510</td>
  <td>5</td>
  <td>13</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=33 CREDITS(THEORY 10 CREDITS, PRACTICE=23 CREDITS) & 885 HOURS</td>
  </tr>
  <tr>
  <td colspan="5">GRADE TOTAL=135 CREDITS</td>
  </tr>
</table>
@endsection
