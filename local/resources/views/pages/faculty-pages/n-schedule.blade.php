@extends('layout.master')
@section('title')
Prossional
@endsection

@section('content')
  <table class="table table-bordered table-condensed">
    <tr class="tb-back">
      <td colspan="5">Year I</td>
    </tr>
    <tr>
      <td colspan="5" class="tr-back">Semester I</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 101 --}}
    <tr>
      <td rowspan="2">Net 101</td>
      <td rowspan="2">C Programing</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 102 --}}
    <tr>
      <td rowspan="2">Net 102</td>
      <td rowspan="2">Basic Networking</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 103 --}}
    <tr>
      <td rowspan="2">Net 103</td>
      <td rowspan="2">Computer Repair</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 104 --}}
    <tr>
      <td rowspan="2">Net 104</td>
      <td rowspan="2">Computer Technology</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 105 --}}
    <tr>
      <td rowspan="2">Net 105</td>
      <td rowspan="2">Computer Application I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 106 --}}
    <tr>
      <td>Net 106</td>
      <td>English for IT I</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>

    {{-- semester II --}}

    <tr>
      <td colspan="5" class="tr-back">Semester II</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 201 --}}
    <tr>
      <td rowspan="2">Net 201</td>
      <td rowspan="2">Window Server I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 202 --}}
    <tr>
      <td rowspan="2">Net 202</td>
      <td rowspan="2">Microtik Router I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 203 --}}
    <tr>
      <td rowspan="2">Net 203</td>
      <td rowspan="2">CISCO SMB I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 204 --}}
    <tr>
      <td rowspan="2">Net 204</td>
      <td rowspan="2">Network Project I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 205 --}}
    <tr>
      <td rowspan="2">Net 205</td>
      <td rowspan="2">Computer Applicatio II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 206 --}}
    <tr>
      <td>Net 206</td>
      <td>English for IT II</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>
    <tr>
      <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 480 HOURS</td>
    </tr>

  {{-- year ii
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year II</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 301 --}}
  <tr>
  <td rowspan="2">Net 301</td>
  <td rowspan="2">Window Server II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 302 --}}
  <tr>
  <td rowspan="2">Net 302</td>
  <td rowspan="2">Mikrotik Router II & CISCO SMB II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 303 --}}
  <tr>
  <td rowspan="2">Net 303</td>
  <td rowspan="2">Network Project</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 304 --}}
  <tr>
  <td rowspan="2">Net 304</td>
  <td rowspan="2">Linux System I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 305 --}}
  <tr>
  <td rowspan="2">Net 305</td>
  <td rowspan="2">Window Server II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 306 --}}
  <tr>
  <td>Net 306</td>
  <td>English for IT II</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 401 --}}
  <tr>
  <td rowspan="2">Net 401</td>
  <td rowspan="2">Window Server III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 402 --}}
  <tr>
  <td rowspan="2">Net 402</td>
  <td rowspan="2">IPCOP & IPFIRE I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 403 --}}
  <tr>
  <td rowspan="2">Net 403</td>
  <td rowspan="2">MS. Exchange Server I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 404 --}}
  <tr>
  <td rowspan="2">Net 404</td>
  <td rowspan="2">Linux System II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 405 --}}
  <tr>
  <td rowspan="2">Net 405</td>
  <td rowspan="2">Research & Methodology</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 406 --}}
  <tr>
  <td>Net 406</td>
  <td>Project Researc</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
  </tr>

  {{-- year III
  ----------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year III</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 501 --}}
  <tr>
  <td rowspan="2">Net 501</td>
  <td rowspan="2">IPCop & IPFire II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 502 --}}
  <tr>
  <td rowspan="2">Net 502</td>
  <td rowspan="2">MS. Exchange Server II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 503 --}}
  <tr>
  <td rowspan="2">Net 503</td>
  <td rowspan="2">Linux System III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 504 --}}
  <tr>
  <td rowspan="2">Net 504</td>
  <td rowspan="2">VMWear Server I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis505 --}}
  <tr>
  <td>Net 505</td>
  <td>English for Business I</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 601 --}}
  <tr>
  <td rowspan="2">Net 601</td>
  <td rowspan="2">Server Clustering I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 602 --}}
  <tr>
  <td rowspan="2">Net 602</td>
  <td rowspan="2">Administrator MS. ForeFront TMG I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 603 --}}
  <tr>
  <td rowspan="2">Net 603</td>
  <td rowspan="2">IT Risk & Control Management I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 604 --}}
  <tr>
  <td rowspan="2">Net 604</td>
  <td rowspan="2">VMWear Server II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 605 --}}
  <tr>
  <td rowspan="2">Net 605</td>
  <td rowspan="2">English for Business II</td>
  <tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=30 CREDITS(THEORY 14 CREDITS, PRACTICE=16 CREDITS) & 690 HOURS</td>
  </tr>

  {{-- year IV
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year IV</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 701 --}}
  <tr>
  <td rowspan="2">Net 701</td>
  <td rowspan="2">Server Clustering II </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 702 --}}
  <tr>
  <td rowspan="2">Net 702</td>
  <td rowspan="2">Administrator MS. ForeFront TMG II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 703 --}}
  <tr>
  <td rowspan="2">Net 703</td>
  <td rowspan="2">IT Risk & Control Management II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 704 --}}
  <tr>
  <td rowspan="2">Net 704</td>
  <td rowspan="2">VMWear Server III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 705 --}}
  <tr>
  <td rowspan="2">Net 705</td>
  <td rowspan="2">CCNA I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>375</td>
  <td>5</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 801 --}}
  <tr>
  <td rowspan="2">Net 801</td>
  <td rowspan="2">Server Vitualize I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 802 --}}
  <tr>
  <td rowspan="2">Net 802</td>
  <td rowspan="2">POSTFix(Cent OS) I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis803 --}}
  <tr>
  <td rowspan="2">Net 803</td>
  <td rowspan="2">IT Assurance & Security Audit I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 804 --}}
  <tr>
  <td rowspan="2">Net 804</td>
  <td rowspan="2">Ethical Hacking I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 805 --}}
  <tr>
  <td rowspan="2">Net 805</td>
  <td rowspan="2">CCNA II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>375</td>
  <td>5</td>
  <td>10</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=30 CREDITS(THEORY 10 CREDITS, PRACTICE=20 CREDITS) & 750 HOURS</td>
  </tr>
  {{-- year V
  --------------------------------------------------------------------------------- --}}
  <tr class="tb-back">
  <td colspan="5">Year V</td>
  </tr>
  <tr>
  <td colspan="5" class="tr-back">Semester I</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 701 --}}
  <tr>
  <td rowspan="2">Net 901</td>
  <td rowspan="2">Server Vitualize II </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 702 --}}
  <tr>
  <td rowspan="2">Net 902</td>
  <td rowspan="2">POSTFix (Cent OS) II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 703 --}}
  <tr>
  <td rowspan="2">Net 903</td>
  <td rowspan="2">IT Assurance & Security Audit II </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 704 --}}
  <tr>
  <td rowspan="2">Net 904</td>
  <td rowspan="2">Ethical Hacking II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 705 --}}
  <tr>
  <td rowspan="2">Net 905</td>
  <td rowspan="2">CCNA III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>375</td>
  <td>5</td>
  <td>10</td>
  </tr>

  {{-- semester II --}}

  <tr>
  <td colspan="5" class="tr-back">Semester II</td>
  </tr>
  <tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
  </tr>
  <tr>
  <td>Theory</td>
  <td>Practice</td>
  </tr>
  {{-- mis 801 --}}
  <tr>
  <td rowspan="2">Net 1001</td>
  <td rowspan="2">POSTFix (Cent OS) III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 802 --}}
  <tr>
  <td rowspan="2">Net 1002</td>
  <td rowspan="2">ICT 4 Development</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis803 --}}
  <tr>
  <td rowspan="2">Net 1003</td>
  <td rowspan="2">GPS Management</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 804 --}}
  <tr>
  <td rowspan="2">Net 1004</td>
  <td rowspan="2">Ethical Hacking III</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>

  {{-- mis 805 --}}
  <tr>
  <td rowspan="2">Net 1005</td>
  <td rowspan="2">CCNA II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  {{-- mis 805 --}}
  <tr>
  <td rowspan="2">Net 1005</td>
  <td rowspan="2">Internship</td>
  <td>135</td>
  <td>0</td>
  <td>3</td>
  </tr>
  <tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
  </tr>
  <tr>
  <td colspan="2">Total:</td>
  <td>510</td>
  <td>5</td>
  <td>13</td>
  </tr>
  <tr>
  <td colspan="5">TOTAL=33 CREDITS(THEORY 10 CREDITS, PRACTICE=23 CREDITS) & 885 HOURS</td>
  </tr>
  <tr>
  <td colspan="5">GRADE TOTAL=165 CREDITS</td>
  </tr>
  </table>
@endsection
