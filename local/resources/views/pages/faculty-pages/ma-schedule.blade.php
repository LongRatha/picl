@extends('layout.master')
@section('title')
  Prossional
@endsection

@section('content')
  <div class="table-responsive">
  <table class="table table-bordered table-condensed">
    <tr class="tb-back">
      <td colspan="5">Year I</td>
    </tr>
    <tr>
      <td colspan="5" class="tr-back">Semester I</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 101 --}}
    <tr>
      <td rowspan="2">MIS 101</td>
      <td rowspan="2">C-Programming</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 102 --}}
    <tr>
      <td rowspan="2">MIS 102</td>
      <td rowspan="2">Basic Networking</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 103 --}}
    <tr>
      <td rowspan="2">MIS 103</td>
      <td rowspan="2">Computer Repair</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 104 --}}
    <tr>
      <td rowspan="2">MIS 104</td>
      <td rowspan="2">Computer Technology</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 105 --}}
    <tr>
      <td rowspan="2">MIS 105</td>
      <td rowspan="2">Computer Application I</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 106 --}}
    <tr>
      <td>MIS 106</td>
      <td>English for IT I</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>

    {{-- semester II --}}

    <tr>
      <td colspan="5" class="tr-back">Semester II</td>
    </tr>
    <tr>
      <td rowspan="2">N<sup>o</sup></td>
      <td rowspan="2">Subject</td>
      <td rowspan="2">Hour</td>
      <td colspan="2">Credit</td>
    </tr>
    <tr>
      <td>Theory</td>
      <td>Practice</td>
    </tr>
    {{-- mis 201 --}}
    <tr>
      <td rowspan="2">MIS 201</td>
      <td rowspan="2">Introduction to Business</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 202 --}}
    <tr>
      <td rowspan="2">MIS 202</td>
      <td rowspan="2">Introduction to Management</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 203 --}}
    <tr>
      <td rowspan="2">MIS 203</td>
      <td rowspan="2">CISCO SMB</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 204 --}}
    <tr>
      <td rowspan="2">MIS 204</td>
      <td rowspan="2">Database Application I & DBMS</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 205 --}}
    <tr>
      <td rowspan="2">MIS 205</td>
      <td rowspan="2">Computer Application II</td>
      <td>15</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <td>60</td>
      <td>0</td>
      <td>2</td>
    </tr>

    {{-- mis 206 --}}
    <tr>
      <td>MIS 206</td>
      <td>English for IT I</td>
      <td>45</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <td colspan="2">Total:</td>
      <td>420</td>
      <td>8</td>
      <td>10</td>
    </tr>
    <tr>
      <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
    </tr>

{{-- year ii
--------------------------------------------------------------------------------- --}}
<tr style="background-color:pink">
  <td colspan="5" class="tb-back">Year II</td>
</tr>
<tr>
  <td colspan="5" class="tr-back">Semester I</td>
</tr>
<tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
</tr>
<tr>
  <td>Theory</td>
  <td>Practice</td>
</tr>
{{-- mis 301 --}}
<tr>
  <td rowspan="2">MIS 301</td>
  <td rowspan="2">Advance of Business</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 302 --}}
<tr>
  <td rowspan="2">MIS 302</td>
  <td rowspan="2">Advance of Management</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 303 --}}
<tr>
  <td rowspan="2">MIS 303</td>
  <td rowspan="2">Network Infrastructor I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 304 --}}
<tr>
  <td rowspan="2">MIS 304</td>
  <td rowspan="2">Database Application II & DBMS</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 305 --}}
<tr>
  <td rowspan="2">MIS 305</td>
  <td rowspan="2">Project Managemnet I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 306 --}}
<tr>
  <td>MIS 306</td>
  <td>English for IT I</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
</tr>
<tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
</tr>

{{-- semester II --}}

<tr>
  <td colspan="5" class="tr-back">Semester II</td>
</tr>
<tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
</tr>
<tr>
  <td>Theory</td>
  <td>Practice</td>
</tr>
{{-- mis 401 --}}
<tr>
  <td rowspan="2">MIS 401</td>
  <td rowspan="2">Data Structure & Algorithm I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 402 --}}
<tr>
  <td rowspan="2">MIS 402</td>
  <td rowspan="2">Statistic & Alnalysis Data I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 403 --}}
<tr>
  <td rowspan="2">MIS 403</td>
  <td rowspan="2">Network Infrastructure I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 404 --}}
<tr>
  <td rowspan="2">MIS 404</td>
  <td rowspan="2">Project Management II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 405 --}}
<tr>
  <td rowspan="2">MIS 405</td>
  <td rowspan="2" style="background-color:red">Data Structure & Algorithm II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 406 --}}
<tr>
  <td>MIS 406</td>
  <td>Research & Methodology</td>
  <td>45</td>
  <td>3</td>
  <td>0</td>
</tr>
<tr>
  <td colspan="2">Total:</td>
  <td>420</td>
  <td>8</td>
  <td>10</td>
</tr>
<tr>
  <td colspan="5">TOTAL=36 CREDITS(THEORY 16 CREDITS, PRACTICE=20 CREDITS) & 840 HOURS</td>
</tr>

{{-- year III
----------------------------------------------------------------------- --}}
<tr style="background-color:pink">
  <td colspan="5" class="tb-back">Year III</td>
</tr>
<tr>
  <td colspan="5" class="tr-back">Semester I</td>
</tr>
<tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
</tr>
<tr>
  <td>Theory</td>
  <td>Practice</td>
</tr>
{{-- mis 501 --}}
<tr>
  <td rowspan="2">MIS 501</td>
  <td rowspan="2">Data Structure & Algorithm</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 502 --}}
<tr>
  <td rowspan="2">MIS 502</td>
  <td rowspan="2">Statistic & Analysis Data II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 503 --}}
<tr>
  <td rowspan="2">MIS 503</td>
  <td rowspan="2">System Analysis I </td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 504 --}}
<tr>
  <td rowspan="2">MIS 504</td>
  <td rowspan="2">Distribute Computer & Cloud Computering I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis505 --}}
<tr>
  <td rowspan="2">MIS 505</td>
  <td rowspan="2">English for Business I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
</tr>

<tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
</tr>

{{-- semester II --}}

<tr>
  <td colspan="5" class="tr-back">Semester II</td>
</tr>
<tr>
  <td rowspan="2">N<sup>o</sup></td>
  <td rowspan="2">Subject</td>
  <td rowspan="2">Hour</td>
  <td colspan="2">Credit</td>
</tr>
<tr>
  <td>Theory</td>
  <td>Practice</td>
</tr>
{{-- mis 601 --}}
<tr>
  <td rowspan="2">MIS 601</td>
  <td rowspan="2">Supply Chain Management I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 602 --}}
<tr>
  <td rowspan="2">MIS 602</td>
  <td rowspan="2">Negotiation & Leadership I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 603 --}}
<tr>
  <td rowspan="2">MIS 603</td>
  <td rowspan="2">System Analysis II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 604 --}}
<tr>
  <td rowspan="2">MIS 604</td>
  <td rowspan="2">Distribute Computer & Cloud Computering I</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>60</td>
  <td>0</td>
  <td>2</td>
</tr>

{{-- mis 605 --}}
<tr>
  <td rowspan="2">MIS 605</td>
  <td rowspan="2">English for Business II</td>
  <td>15</td>
  <td>1</td>
  <td>0</td>
</tr>
<tr>
  <td>45</td>
  <td>3</td>
  <td>0</td>
</tr>
<tr>
  <td colspan="2">Total:</td>
  <td>345</td>
  <td>7</td>
  <td>8</td>
</tr>
<tr>
  <td colspan="5">TOTAL=30 CREDITS(THEORY 14 CREDITS, PRACTICE=16 CREDITS) & 690 HOURS</td>
</tr>

{{-- year IV
--------------------------------------------------------------------------------- --}}
<tr style="background-color:pink">
<td colspan="5" class="tb-back">Year IV</td>
</tr>
<tr>
<td colspan="5" class="tr-back">Semester I</td>
</tr>
<tr>
<td rowspan="2">N<sup>o</sup></td>
<td rowspan="2">Subject</td>
<td rowspan="2">Hour</td>
<td colspan="2">Credit</td>
</tr>
<tr>
<td>Theory</td>
<td>Practice</td>
</tr>
{{-- mis 701 --}}
<tr>
<td rowspan="2">MIS 701</td>
<td rowspan="2">Supply Chain Management II</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 702 --}}
<tr>
<td rowspan="2">MIS 702</td>
<td rowspan="2">Negotiation & Leadership II</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 703 --}}
<tr>
<td rowspan="2">MIS 703</td>
<td rowspan="2">IT Risk & Control Management I</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 704 --}}
<tr>
<td rowspan="2">MIS 704</td>
<td rowspan="2">Principle of Data Wherehousing</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 705 --}}
<tr>
<td rowspan="2">MIS 705</td>
<td rowspan="2">Ethical Hacking</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>
<tr>
<td colspan="2">Total:</td>
<td>345</td>
<td>5</td>
<td>10</td>
</tr>

{{-- semester II --}}

<tr>
<td colspan="5" class="tr-back">Semester II</td>
</tr>
<tr>
<td rowspan="2">N<sup>o</sup></td>
<td rowspan="2">Subject</td>
<td rowspan="2">Hour</td>
<td colspan="2">Credit</td>
</tr>
<tr>
<td>Theory</td>
<td>Practice</td>
</tr>
{{-- mis 801 --}}
<tr>
<td rowspan="2">MIS 801</td>
<td rowspan="2">Social Analysis with Business</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 802 --}}
<tr>
<td rowspan="2">MIS 802</td>
<td rowspan="2">Principle of Data Wherehousing</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis803 --}}
<tr>
<td rowspan="2">MIS 803</td>
<td rowspan="2">IT Risk & Control Management II</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 804 --}}
<tr>
<td rowspan="2">MIS 804</td>
<td rowspan="2">Ethical Hacking II</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 805 --}}
<tr>
<td rowspan="2">MIS 805</td>
<td rowspan="2">Marketing Management & Reserch</td>
<td>15</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>60</td>
<td>0</td>
<td>2</td>
</tr>

{{-- mis 806 --}}
<tr>
<td>MIS 806</td>
<td>Inership</td>
<td>135</td>
<td>0</td>
<td>3</td>
</tr>
<tr>
<td colspan="2">Total:</td>
<td>510</td>
<td>5</td>
<td>13</td>
</tr>
<tr>
<td colspan="5">TOTAL=33 CREDITS(THEORY 10 CREDITS, PRACTICE=23 CREDITS) & 855 HOURS</td>
</tr>
<tr>
<td colspan="5">GRADE TOTAL=135 CREDITS</td>
</tr>

  </table>
</div>
@endsection
