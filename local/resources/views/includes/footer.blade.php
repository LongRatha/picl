<div class="container-fluid">
  <div class="row footer">
    <div class="container">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <img src="img/picl_logo.png" width="300px" height="65px" class="img-responsive" style="margin:20px 0px;">
        <!--<h4>Professional Institue of Computer & Language</h4>-->
        <ul style="list-style-type:none;" class="out">
          <li><a href="{{url('/')}}" ><span>ទំព័រដើម</span></a></li>
          <li><a href="{{url('/program')}}"><span>កម្មវិធីសិក្សា</span></a></li>
          <li><a href="{{url('/news')}}"><span>ព័ត៌មាន & ព្រឹត្តិការណ៍ថ្មីៗ</span></a></li>
          <li><a href="{{url('/career')}}"><span>ដំណឹងជ្រើសរើសបុគ្គលិក</span></a></li>
          <li><a href="{{url('/contact')}}"><span>ទំនាក់ទំនង</span></a></li>
          <li><a href="{{url('/about')}}"><span>អំពីយើង</span></a></li>
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <center><h3>Join mail with us</h3>
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-info" type="button">Summit</button>
          </span>
        </div>
        <div class="img-responsive">
          <h3>បណ្តាញសង្គម</h3>
          <a href="www.facebook.com/piclinstitute"><img src='img/facebook-color.png'alt="Facebook" /></a>
          <a href="#"><img src='img/color-nfo-logo.png'alt="Goolle+" /></a>
        </div>
        </center>
      </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <center><h3>ទំនាក់ទំនង</h3></center>
          <i class="glyphicon glyphicon-map-marker"></i><span> building 143 | st 273 sangkat Toul,sangke khan Russei Keo,Phnom Penh </span>
          <br>
          <i class="glyphicon glyphicon-earphone"></i><span> Tel: 086 96 97 65 / 012 58 99 33 / 066 23 47 77</span>
          <br>
          <i class="glyphicon glyphicon-globe"></i><span> Website: www.picl-institute.com
          </span>
          <br />
          <i class="glyphicon glyphicon-envelope"></i><span> picl.edu@yahoo.com</span>
        </div>
        <!-- /input-group -->
      </div>
    </div>
  </div>
