<div class="container">
    <div class="row">
    	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="img/6.jpg" alt="PICL">
      <div class="carousel-caption">
        Programming
      </div>
    </div>
    <div class="item">
      <img src="img/2.jpg" alt="PICL">
      <div class="carousel-caption">
        Networking
      </div>
    </div>
    <div class="item">
      <img src="img/3.jpg" alt="PICL">
      <div class="carousel-caption">
        English
      </div>
    </div>
    <div class="item">
      <img src="img/4.jpg" alt="PICL">
      <div class="carousel-caption">
      Business
      </div>
    </div>
    <div class="item">
      <img src="img/5.jpg" alt="PICL">
      <div class="carousel-caption">
        Korea
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

   </div>
</div>
