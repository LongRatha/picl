<div class="row backcolor">
  <center><h3><span>ITEC CENTER</span></h3></center>
  <ul class="nav nav-stacked">
    <li class="{{ Request::path() == 'itec_program' ? 'bcolor' : '' }}"><a href="{{url('/itec_program')}}"><span><i class="glyphicon glyphicon-phone" aria-hidden="true"></i> សរសេរកម្មវិធី</span></a></li>
    <li class="{{ Request::path() == 'itec_website' ? 'bcolor' : '' }}"><a href="{{url('/itec_website')}}"><span><i class="glyphicon glyphicon-globe" aria-hidden="true"></i> សរសេរវេបសាយ</span></a></li>
    <li class="{{ Request::path() == 'itec_network' ? 'bcolor' : '' }}"><a href="{{url('/itec_network')}}"><span><i class="glyphicon glyphicon-wrench" aria-hidden="true"></i> តបណ្តាញ</span></a></li>
    <li class="{{ Request::path() == 'itec_design' ? 'bcolor' : '' }}"><a href="{{url('/itec_design')}}"><span><i class="glyphicon glyphicon-edit" aria-hidden="true"></i> កាត់តរូបភាព</span></a></li>
  </ul>
</div>
