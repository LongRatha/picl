
<div class="row  backcolor" id="faculty">
  <div id="accordion">
    <center>
      <h3><span>មហាវិទ្យាល័យ</span></h3>
    </center>
    <h4><span>&nbsp;<i class="glyphicon glyphicon-minus"></i> វិស្វកម្មវិទ្យាសាស្រ្តកុំព្យូទ័រ</span></h4>
    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right" ></i>
        <a data-toggle="collapse" href="#menuPanelListGroup"><span>សរសេរកម្មវិធី</span>
        </a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menuPanelListGroup">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/p-professional')}}"> <span>Professional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/p-schedule')}}" ><span>Schedule</span></a>
      </li>

    </ul>
    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup1"><span>ភ្ជាប់បណ្តាញ</span></a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup1">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/n-professional')}}"><span>Profesional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/n-schedule')}}"><span>Schedule</span></a>
      </li>
    </ul>
    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right"></i><a data-toggle="collapse" href="#menuPanelListGroup1"><span> សេដ្ឋកិច្ចព័ត៌មានវិទ្យា</span></a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menuPanelListGroup1">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/b-professional')}}" ><span>Profesional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/b-schedule')}}"><span>Schedule</span></a>
      </li>
    </ul>
    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup"><span>គ្រប់គ្រងប្រព័ន្ធព័ត៌មាន</span></a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/ma-professional')}}"><span>Profesional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/ma-schedule')}}"><span>Schedule</span></a>
      </li>
    </ul>

    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup3"><span>កាត់តរូបភាព​ & វីដេអូ</span></a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup3">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/g-professional')}}"><span>Profesional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/g-schedule')}}"><span>Schedule</span></a>
      </li>
    </ul>
    <h4>&nbsp;<i class="glyphicon glyphicon-minus"></i><span>​ ភាសា</span></h4>
    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup5"><span>អង់គ្លេស</span></a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup5">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/e-professional')}}"><span>Profesional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/e-schedule')}}"><span>Schedule</span></a>
      </li>
    </ul>
    <div class="panel-heading librePanelHeading">
      <div class="panel-title">
        <i class="indicator glyphicon glyphicon-chevron-right"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup6"><span>កូរ៉េ</span></a>
      </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup6">
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/k-professional')}}"><span>Profesional</span></a>
      </li>
      <li class="list-group-item librePanelListGroupItem">
        <a href="{{url('/k-schedule')}}"><span>Schedule</span></a>
      </li>
    </ul>
  </center>
  </div>

</div>
