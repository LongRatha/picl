
<div class="row backcolor" >
  <!--<div class="container" >-->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <center><h3><span>PICL INSTITUTE</span></h3></center>
          <ul class="nav nav-stacked">
            <li class="{{ Request::path() == '/' ? 'bcolor' : '' }}" ><a href="{{url('/')}}"><span><i class="glyphicon glyphicon-home" aria-hidden="true"></i> ទំព័រដើម</span></a></li>
            <li class="{{ Request::path() == 'program' ? 'bcolor' : '' }}">
              <a href="{{url('/program')}}"><span><i class="glyphicon glyphicon-book" aria-hidden="true"></i> កម្មវិធីសិក្សា</span></a></li>
            <li class="{{ Request::path() == 'news' ? 'bcolor' : '' }}"><a href="{{url('/news')}}"><span><i class="glyphicon glyphicon-calendar" aria-hidden="true"></i> ព័ត៌មាន & ព្រឹត្តិការណ៍ថ្មីៗ</span></a></li>
            <li class="{{ Request::path() == 'career' ? 'bcolor' : '' }}"><a href="{{url('/career')}}"><span><i class="glyphicon glyphicon-bullhorn" aria-hidden="true"></i> ដំណឹងជ្រើសរើសបុគ្គលិក</span></a></li>
            <li class="{{ Request::path() == 'contact' ? 'bcolor' : '' }}"><a href="{{url('/contact')}}"><span><i class="glyphicon glyphicon-phone-alt" aria-hidden="true"></i> ទំនាក់ទំនង</span></a></li>
            <li class="{{ Request::path() == 'about' ? 'bcolor' : '' }}"><a href="{{url('/about')}}"><span><i class="glyphicon glyphicon-user" aria-hidden="true"></i> អំពីយើង</span></a></li>
          </ul>
</div>
