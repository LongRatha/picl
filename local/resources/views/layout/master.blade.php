<html>
<head>
  <meta name="keywords" content="picl institute, itec, information technology, software development, website development, android, ios, networking">
  <meta name="description" content="Welcome to Professional Institute of Computer&Language (PICL). PICL is one of the most institute public in Cambodia.....">
  <link rel="shortcut icon" href="img/shortcut-logo.png" />
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <script src="{{ asset('js/jquery-3.1.1.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/jquery.collapse.js') }}" charset="utf-8">
  <script src="{{ asset('js/custom.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/backtotop.js') }}" charset="utf-8"></script>


  <title>PICL Institute @yield('title')</title>
</head>
<body>
  <a href="#" id="back-to-top" title="Back to top" ><img src='img/Back2Top .png' /></a>
  @include('includes.header')<br>
  {{-- @include('includes.menu') --}}
  {{-- @include('includes.slide')<br> --}}
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
        {{-- @include('includes.leftmenu') --}}
        @include('includes.menu')<br>
        @include('includes.itec')<br>
        {{-- @include('includes.partner')
        <br />
        @include('includes.scholarship')
        <br/>
        @include('includes.shift') --}}
      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @yield('img')
          </div>
        </div>
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          @yield('content')
          </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            @include('includes.right')
          </div>
        </div>
      </div>

      {{-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
      {{-- @include('includes.right') --}}
    </div>
  </div><br>
  @include('includes.footer')
</body>
</html>
