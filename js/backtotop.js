// using for scroll to top window

$(document).ready(function(){
    var btt = $('#back-to-top');

    btt.on('click', function(){
        $('html, body').animate({
            scrollTop:0
        }, 700);
    });

    $(window).on('scroll', function(){
        var self = $(this),
            height = self.height(),
            top = self.scrollTop();

        if(top > height/8){
            if(!btt.is(':visible')){
                btt.fadeIn();
            }
        }else{
            btt.fadeOut();
        }

    });
});
